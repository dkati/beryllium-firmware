# Firmware
PRODUCT_COPY_FILES += \
    vendor/beryllium-firmware/abl.elf:install/firmware-update/abl.elf \
    vendor/beryllium-firmware/aop.img:install/firmware-update/aop.img \
    vendor/beryllium-firmware/bluetooth.img:install/firmware-update/bluetooth.img \
    vendor/beryllium-firmware/cmnlib.img:install/firmware-update/cmnlib.img \
    vendor/beryllium-firmware/cmnlib64.img:install/firmware-update/cmnlib64.img \
    vendor/beryllium-firmware/devcfg.img:install/firmware-update/devcfg.img \
    vendor/beryllium-firmware/dsp.img:install/firmware-update/dsp.img \
    vendor/beryllium-firmware/hyp.img:install/firmware-update/hyp.img \
    vendor/beryllium-firmware/keymaster.img:install/firmware-update/keymaster.img \
    vendor/beryllium-firmware/logo.img:install/firmware-update/logo.img \
    vendor/beryllium-firmware/modem.img:install/firmware-update/modem.img \
    vendor/beryllium-firmware/qupfw.img:install/firmware-update/qupfw.img \
    vendor/beryllium-firmware/storsec.img:install/firmware-update/storsec.img \
    vendor/beryllium-firmware/tz.img:install/firmware-update/tz.img \
    vendor/beryllium-firmware/xbl_config.img:install/firmware-update/xbl_config.img \
    vendor/beryllium-firmware/xbl.img:install/firmware-update/xbl.img
